const OPEN_CREATE_DIRECTORY_MODAL = 'ui/OPEN_CREATE_DIRECTORY_MODAL'
const CLOSE_CREATE_DIRECTORY_MODAL = 'ui/CLOSE_CREATE_DIRECTORY_MODAL'

export default function reducer(state = {}, action = {}) {
    console.log(state)
    switch (action.type) {
        case OPEN_CREATE_DIRECTORY_MODAL:
        case CLOSE_CREATE_DIRECTORY_MODAL:
            return { ...state, create_directory_modal_is_open: action.value }
        default:
            return state
    }
}

export function openCreateDirectoryModal() {
    return { type: OPEN_CREATE_DIRECTORY_MODAL, value: true }
}

export function closeCreateDirectoryModal() {
    return { type: CLOSE_CREATE_DIRECTORY_MODAL, value: false }
}
