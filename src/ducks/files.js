const CREATE_DIRECTORY = 'files/CREATE_DIRECTORY'

export default function reducer(state = { files: [], selected: null, directory: '/' }, action = {}) {
    switch (action.type) {
        case CREATE_DIRECTORY:
            return {
                ...state,
                files: [
                    ...state.files,
                    { path: action.directory, mimetype: 'inode/directory' }
                ]
            }
        default:
            return state
    }
}

export function createDirectory(directory) {
    return { type: CREATE_DIRECTORY, directory }
}
