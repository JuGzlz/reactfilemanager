const SELECT = 'roots/SELECT'

export default function reducer(state = { roots: [], current: null }, action = {}) {
    switch (action.type) {
        case SELECT:
            return { ...state, current: action.value }
        default:
            return state
    }
}

export function selectRoot(value) {
    return { type: SELECT, value }
}
