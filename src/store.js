import {createStore} from 'react-redux-app-container'
import filesReducer from 'ducks/files'
import rootsReducer from 'ducks/roots'
import uiReducer from 'ducks/ui'

export default () => {
    const initialState = {
        files: {
            files: [
                {
                    path: 'local://test/',
                    mimetype: 'inode/directory',
                },
                {
                    path: 'local://3.jpg',
                    mimetype: 'image/jpeg',
                }, {
                    path: 'local://4.jpg',
                    mimetype: 'image/jpeg',
                },
            ],
            selectedFile: null,
            selectedFiles: [],
            directory: '/',
        },
        roots: {
            roots: [
                {
                    path: 'local://',
                },
                {
                    path: 'amazon://',
                },
            ],
            current: {
                path: 'local://',
            },
        },
        ui: {
            create_directory_modal_is_open: false,
        }
    }
    const reducers = {
        files: filesReducer,
        roots: rootsReducer,
        ui: uiReducer,
    }
    const middlewares = []
    const enhancers = []

    return createStore(initialState, reducers, middlewares, enhancers)
}
