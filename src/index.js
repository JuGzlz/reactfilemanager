import React from 'react'
import ReactDOM from 'react-dom'
import { ReactReduxAppContainer } from 'react-redux-app-container'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import createStore from './store'

const MOUNT_NODE = document.getElementById('root')

const store = createStore()

ReactDOM.render(
    <ReactReduxAppContainer store={store}>
        <App />
    </ReactReduxAppContainer>,
    MOUNT_NODE
)
registerServiceWorker()
