import React, { Component } from 'react'
import ToolBar from 'components/ToolBar'
import TreeSidebar from 'components/TreeSidebar'
import FilesList from 'components/FilesList'

class App extends Component {
    render() {
        return (
            <div>
                <h2>Files</h2>
                <ToolBar />
                <TreeSidebar />
                <FilesList />
            </div>
        )
    }
}

export default App
