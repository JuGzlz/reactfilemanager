import React from 'react'
import PropTypes from 'prop-types'
import FileType from 'types/file'

const FilesList = ({ files }) => (
    <ul>
        {files.map((file) => {
            return (
                <li key={file.path}>{file.path}</li>
            )
        })}
    </ul>
)

FilesList.propTypes = {
    files: PropTypes.arrayOf(FileType).isRequired,
}

export default FilesList
