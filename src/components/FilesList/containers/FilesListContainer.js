import { connect } from 'react-redux'
import FilesList from '../components/FilesList'

const mapDispatchToProps = {}

const mapStateToProps = (state) => ({
    files: state.files.files,
})

export default connect(mapStateToProps, mapDispatchToProps)(FilesList)
