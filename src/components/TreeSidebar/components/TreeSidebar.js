import React from 'react'
import PropTypes from 'prop-types'
import RootType from 'types/root'

const TreeSidebar = ({roots, selectRoot}) => (
    <ul>
        {roots.map((root) => {
            return (
                <li key={root.path}>
                    <button onClick={() => selectRoot(root)}>
                        {root.path}
                    </button>
                </li>
            )
        })}
    </ul>
)

TreeSidebar.propTypes = {
    roots: PropTypes.arrayOf(RootType).isRequired,
    selectRoot: PropTypes.func.isRequired,
}

export default TreeSidebar
