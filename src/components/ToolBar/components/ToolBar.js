import React, {Component} from 'react'
//import PropTypes from 'prop-types'
import CreateDirectoryForm from '../containers/CreateDirectoryFormContainer'


class ToolBar extends Component {
    constructor(props) {
        super(props)

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        console.log(event.target.files)
        //this.setState({value: event.target.value});
    }

    render() {
        let { createDirectoryModalIsOpen, openCreateDirectoryModal } = this.props

        return (
            <ul>
                <li>
                    <button onClick={() => openCreateDirectoryModal() }>
                        Create Directory
                    </button>
                    {createDirectoryModalIsOpen &&
                    <CreateDirectoryForm />
                    }
                    <label>
                        <input
                            multiple="multiple"
                            onChange={this.handleChange}
                            type="file"/>
                    </label>
                </li>
            </ul>
        )
    }
}

//ToolBar.propTypes = {}

export default ToolBar
