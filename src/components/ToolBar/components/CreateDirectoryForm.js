import React, {Component} from 'react'
import PropTypes from 'prop-types'
import RootType from 'types/root'


class CreateDirectoryForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: ''
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        this.setState({ value: event.target.value })
    }

    handleSubmit(event) {
        event.preventDefault()
        this.props.createDirectory(this.props.currentRoot.path + this.state.value)
        this.props.closeCreateDirectoryModal()
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="text" value={this.state.value} onChange={this.handleChange}/>
                <input type="submit" value="Submit"/>
            </form>
        )
    }
}


CreateDirectoryForm.propTypes = {
    createDirectory: PropTypes.func.isRequired,
    currentRoot: RootType.isRequired,
}

export default CreateDirectoryForm
