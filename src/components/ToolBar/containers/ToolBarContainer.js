import {connect} from 'react-redux'
import ToolBar from '../components/ToolBar'
import {openCreateDirectoryModal} from 'ducks/ui'

const mapDispatchToProps = {
    openCreateDirectoryModal
}

const mapStateToProps = (state) => ({
    createDirectoryModalIsOpen: state.ui.create_directory_modal_is_open,
})

export default connect(mapStateToProps, mapDispatchToProps)(ToolBar)
