import {connect} from 'react-redux'
import {createDirectory} from 'ducks/files'
import {closeCreateDirectoryModal} from 'ducks/ui'
import CreateDirectoryForm from '../components/CreateDirectoryForm'

const mapDispatchToProps = {
    createDirectory,
    closeCreateDirectoryModal,
}

const mapStateToProps = (state) => ({
    currentRoot: state.roots.current,
    createDirectoryModalIsOpen: state.ui.create_directory_modal_is_open,
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateDirectoryForm)
